package com.example.contactapp

import android.content.ContentResolver
import android.database.Cursor
import android.provider.ContactsContract
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {
    var listViewSelected = false
    var listShowed = false
    lateinit var cursor : Cursor
    var cursorInitialized = false

    fun initializeCursor(contentResolver : ContentResolver) {
        val projection = arrayOf(
            ContactsContract.CommonDataKinds.Phone._ID,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.PHOTO_URI
        )

        cursor = contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            projection,
            null, null, null
        )!!

        cursor.moveToFirst()
        cursorInitialized = true
    }
}