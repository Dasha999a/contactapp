package com.example.contactapp

import android.database.Cursor
import android.net.Uri
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ContactAdapter(private val cursor : Cursor,
                     private val listener: OnClickListener)
    : RecyclerView.Adapter<ContactAdapter.ViewHolder>() {

    class ViewHolder(val view : View) : RecyclerView.ViewHolder(view) {
        val contactPhoto : ImageView = view.findViewById(R.id.contact_photo)
        val contactName : TextView = view.findViewById(R.id.contact_name)
        val contactPhone : TextView = view.findViewById(R.id.contact_phone)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.contact_holder, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        cursor.moveToPosition(position)

        val photoUriColumn =
            cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)
        val photoUri = cursor.getString(photoUriColumn)

        if (photoUri != null) holder.contactPhoto.setImageURI(Uri.parse(photoUri))
        else holder.contactPhoto.setImageResource(R.drawable.ic_person)


        val nameColumn =
            cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
        holder.contactName.text = cursor.getString(nameColumn)


        val phoneColumn =
            cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
        holder.contactPhone.text = cursor.getString(phoneColumn)


        holder.view.setOnClickListener(listener)
    }

    override fun getItemCount() = cursor.count
}