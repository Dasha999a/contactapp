package com.example.contactapp

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.CursorAdapter
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.contactapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), OnClickListener {
    private lateinit var binding : ActivityMainBinding
    private lateinit var actionBarDrawerToggle : ActionBarDrawerToggle
    private lateinit var viewModel : MainViewModel
    private lateinit var fragmentContainer : Fragment

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
            requestCode == 0) {
            if (!viewModel.cursorInitialized) viewModel.initializeCursor(contentResolver)
            selectListType()
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this)[MainViewModel::class.java]

        val drawerLayout = binding.drawerLayout
        val navigationView = binding.navigationView
        actionBarDrawerToggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            R.string.open,
            R.string.close
        )

        navigationView.setNavigationItemSelectedListener {
            viewModel.listViewSelected = when(it.itemId) {
                R.id.list_view -> true
                else -> false
            }

            drawerLayout.close()
            viewModel.listShowed = true
            checkPermission()
            true
        }

        drawerLayout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onResume() {
        super.onResume()
        checkPermission()
    }

    private fun checkPermission() {
        if (viewModel.listShowed) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED) {
                if (!viewModel.cursorInitialized) viewModel.initializeCursor(contentResolver)
                selectListType()
            } else {
                requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), 0)
            }
        }
    }

    private fun selectListType() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container_view,
                if (viewModel.listViewSelected) ListViewFragment() else RecyclerViewFragment()
            )
            .commitNow()
        initializeLists(viewModel.cursor)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            true
        } else super.onOptionsItemSelected(item)
    }

    private fun initializeListView(cursor : Cursor) {
        val adapter = object : CursorAdapter(this, cursor, true) {
            override fun newView(context: Context?, cursor: Cursor?, parent: ViewGroup?): View {
                return LayoutInflater.from(context)
                    .inflate(R.layout.contact_holder, parent, false)
            }

            override fun bindView(view: View?, context: Context?, cursor: Cursor?) {
                val photoUriColumn =
                    cursor?.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)
                val photoUri = photoUriColumn?.let { cursor.getString(it) }
                val contactPhoto : ImageView? = view?.findViewById(R.id.contact_photo)

                if (photoUri != null) contactPhoto?.setImageURI(Uri.parse(photoUri))
                else contactPhoto?.setImageResource(R.drawable.ic_person)


                val nameColumn =
                    cursor?.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                val name = nameColumn?.let { cursor.getString(it) }
                view?.findViewById<TextView>(R.id.contact_name)?.text = name


                val phoneColumn =
                    cursor?.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                val phone = phoneColumn?.let { cursor.getString(phoneColumn) }
                view?.findViewById<TextView>(R.id.contact_phone)?.text = phone
            }
        }

        val listView = fragmentContainer.view?.findViewById<ListView>(R.id.list_view)
        listView?.adapter = adapter

        listView?.setOnItemClickListener { _, view, _, _ ->
            val phone = view.findViewById<TextView>(R.id.contact_phone).text
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:$phone")
            startActivity(intent)
        }
    }

    private fun initializeRecyclerView(cursor : Cursor) {
        val recyclerView =
            fragmentContainer.view?.findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView?.adapter = ContactAdapter(cursor, this)
        recyclerView?.layoutManager = LinearLayoutManager(this)
    }

    private fun initializeLists(cursor : Cursor) {
        fragmentContainer = supportFragmentManager.findFragmentById(R.id.fragment_container_view)!!

        if (viewModel.listViewSelected) initializeListView(cursor)
        else initializeRecyclerView(cursor)
    }

    override fun onClick(v: View?) {
        val phone = v?.findViewById<TextView>(R.id.contact_phone)?.text
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phone")
        startActivity(intent)
    }
}